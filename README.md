# Content Transformation Module

Drupal's model is based on "entities," which loosely translate to tables. Entities may contain fields directly or contain "bundles", which are collections of fields.

This module takes one type of entity and transforms it into a different entity type.

When a user is on the edit page for entity type A, the form may or may not display a button, based on the user's permissions. This is represented by `Button/CollectionButton`.

When the user clicks the button, the form hits a route controlled by `Controller/CollectionFactoryController`. This kicks off an attempt at transforming an instance of entity type A into entity type B (Collection). On success, it routes the user to the edit form for the newly created instance of Collection. On failure, it returns the user to the edit form for the instance of entity type A.

In the future, we may want to convert other types of entities to Collection. I have made an effort to abstract the logic and utilize metaprogramming where possible.