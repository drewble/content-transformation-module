<?php

namespace Drupal\Tests\collection_from_entity\Unit;

use Drupal\collection_from_entity\CollectionFactory\CollectionFromNarrative;
use Drupal\collection_from_entity\CollectionFactory\CollectionItem;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\lonely_planet_content\FormattedTextParser;

/**
 * Mocked methods for CollectionFromNarrative testing.
 */
class CollectionFromNarrativeMock extends CollectionFromNarrative {
  /**
   * The mocked POI objects.
   *
   * @var array
   */
  private $pois;

  /**
   * The mocked Paragraph objects.
   *
   * @var array
   */
  private $paragraphs;

  /**
   * Constructor for mock Narrative.
   */
  public function __construct(FormattedTextParser $formattedTextParser, AccountProxyInterface $currentUser, Connection $database, EntityTypeManager $entityTypeManager, LoggerChannelFactoryInterface $logger) {
    parent::__construct($formattedTextParser, $currentUser, $database, $entityTypeManager, $logger);

    // 'narrative' bundle is not supported
    $this->pois = [
      new EntityStub('test poi 1', '1', 'poi'),
      new EntityStub('test poi 2', '2', 'poi'),
      new EntityStub('test poi 3', '3', 'poi'),
      new EntityStub('test narrative 1', '1', 'narrative'),
      new EntityStub('test poi 5', '5', 'poi'),
      new EntityStub('test poi 6', '6', 'poi'),
      new EntityStub('test poi 7', '7', 'poi'),
    ];

    // 'narrative' item should be weeded out
    $this->paragraphs = [
      ['1', '1'],
      ['2', '2'],
      ['3', '3'],
      ['5', '5'],
      ['6', '6'],
      ['7', '7'],
    ];

  }

  /**
   * Return the mocked entity.
   *
   * @return EntityStub
   *   The mocked entity.
   */
  public function getEntity($_entityType, $_entityId) {
    return array_shift($this->pois);
  }

  /**
   * Return the mocked item ID.
   *
   * @return string
   *   The mocked id.
   */
  public function createItem(CollectionItem $item) {
    return array_shift($this->paragraphs);
  }

}
