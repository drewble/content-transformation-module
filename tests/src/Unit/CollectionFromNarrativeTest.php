<?php

namespace Drupal\Tests\collection_from_entity\Unit;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Session\AccountProxy;
use Drupal\lonely_planet_content\FormattedTextParser;
use Drupal\Tests\UnitTestCase;

/**
 * Test CollectionFromNarrative class.
 *
 * @group collection_from_entity
 */
class CollectionFromNarrativeTest extends UnitTestCase {

  /**
   * The collectionFromNarrative class.
   *
   * @var \Drupal\collection_from_entity\CollectionFactory\CollectionFromNarrative
   */
  public $collectionFromNarrative;

  /**
   * The stubbed narrative object.
   *
   * @var NarrativeStub
   */
  private $narrative;

  /**
   * The mocked formatted_text_parser service.
   *
   * @var PHPUnit_Framework_MockObject_MockObject
   */
  private $formattedTextParser;

  /**
   * The mocked database service.
   *
   * @var PHPUnit_Framework_MockObject_MockObject
   */
  private $database;

  /**
   * The mocked current_user service.
   *
   * @var PHPUnit_Framework_MockObject_MockObject
   */
  private $accountProxy;

  /**
   * The mocked entity_type.manager service.
   *
   * @var PHPUnit_Framework_MockObject_MockObject
   */
  private $entityTypeManager;

  /**
   * The mocked logger.factory service.
   *
   * @var PHPUnit_Framework_MockObject_MockObject
   */
  private $loggerFactory;

  /**
   * The test container.
   *
   * @var \Drupal\Core\DependencyInjection\ContainerBuilder
   */
  private $container;

  /**
   * Set up narrative stub.
   *
   * @return NarrativeStub
   *   The stubbed Narrative object.
   */
  private function getNarrativeStub() {
    $content = "<p>With 12,000 and counting, there is a temple for every god and occasion. Storehouses of history, display rooms for decorative arts and, of course, vibrant houses of worship, temples are a quintessential part of Taiwan’s living folk culture.</p><p><a class=\"poi\" href=\"/pois/1358333\">Longshan Temple</a> This graceful walled temple in Lukang is a treasure house of woodcarving and design.</p><p><a class=\"poi\" href=\"/pois/375583\">Bao'an Temple</a> A top example of southern temple art and architecture that won Unesco recognition for its restoration.</p><p><a class=\"poi\" href=\"/pois/1357795\">Tzushr Temple</a> The temple’s post-WWII reconstruction was overseen by an art professor – and it shows.</p><p><a class=\"page\" href=\"/narratives/af137689-9f05-41e0-860e-e1f7d8b01dd2\">Lu'ermen Temples</a> The Orthodox Lu'ermen Mazu Temple and the Lu'ermen Tianhou Temple are two of the most important temples in Taiwan, with relics from Koxinga's original battleship.</p><p><a class=\"poi\" href=\"/pois/435082\">Tainan Confucius Temple</a> Taiwan’s first Confucius temple and a model of graceful design and dignified atmosphere.</p><p><a class=\"poi\" href=\"/pois/435080\">City God Temple</a> Your moral character will be scrutinised at the home of Taiwan's most famous temple plaque: 'You're here at last'.</p><p><a class=\"poi\" href=\"/pois/1063089\">Chung Tai Chan Temple</a> The casino-meets-mosque exterior belies an interior filled with tradition-inspired decorative arts.</p>";
    $introduction = "With 12,000 and counting, there is a temple for every god and occasion. Storehouses of history, display rooms for decorative arts and, of course, vibrant houses of worship, temples are a quintessential part of Taiwan’s living folk culture.";
    return new NarrativeStub("Test Narrative", $content, $introduction, "1", "1");
  }

  /**
   * Set up the test.
   */
  public function setUp() {
    parent::setUp();

    $this->narrative = $this->getNarrativeStub();

    $this->container = new ContainerBuilder();

    $this->database = $this->createMock(Connection::class);
    $this->container->set("database", $this->database);

    $this->accountProxy = $this->getMockBuilder(AccountProxy::class)->setMethods(['id'])->getMock();
    $this->accountProxy->method('id')->willReturn('1');
    $this->container->set("current_user", $this->accountProxy);

    $this->entityTypeManager = $this->createMock(EntityTypeManager::class);
    $this->container->set("entity_type.manager", $this->entityTypeManager);

    $this->formattedTextParser = FormattedTextParser::create($this->container);
    $this->container->set("lonely_planet_content.formatted_text_parser", $this->formattedTextParser);

    $this->loggerFactory = $this->createMock(LoggerChannelFactory::class);
    $this->container->set("logger.factory", $this->loggerFactory);

    $this->collectionFromNarrative = CollectionFromNarrativeMock::create($this->container);
  }

  /**
   * Test the load() and get() methods.
   *
   * @throws \Drupal\collection_from_entity\Exception\PropertyNotFound
   */
  public function testLoadAndGet() {
    $this->collectionFromNarrative->load($this->narrative);

    $this->assertEquals($this->narrative->getTitle(), $this->collectionFromNarrative->get('title'));
    $this->assertEquals($this->narrative->get('introduction'), $this->collectionFromNarrative->get('introduction'));
    $this->assertEquals($this->narrative->getOwnerId(), $this->collectionFromNarrative->get('publisher'));
    $this->assertEquals('Generator', get_class($this->collectionFromNarrative->get('items')));
    $this->assertEquals($this->narrative->get('field_nar_place'), $this->collectionFromNarrative->get('place'));
  }

  /**
   * Test the formatCollectionValues() method.
   *
   * This method is the last bit of logic before creating
   * and saving the new Collection and will test all of the
   * logic required to transform a narrative into a collection.
   */
  public function testFormatCollectionValues() {
    $this->assertTrue(method_exists($this->collectionFromNarrative, 'createCollection'));

    $this->collectionFromNarrative->load($this->narrative);

    $reflectionClass = new \ReflectionClass($this->collectionFromNarrative);
    $reflectionMethod = $reflectionClass->getMethod('formatCollectionValues');
    $reflectionMethod->setAccessible(TRUE);
    $expectedFormattedValues = [
      'type' => 'collection',
      'name' => $this->collectionFromNarrative->get('title'),
      'field_introduction' => $this->collectionFromNarrative->get('introduction'),
      'field_collection_place' => $this->collectionFromNarrative->get('place'),
      'field_collection_items' => [
        [
          'target_id' => '1',
          'target_revision_id' => '1',
        ],
        [
          'target_id' => '2',
          'target_revision_id' => '2',
        ],
        [
          'target_id' => '3',
          'target_revision_id' => '3',
        ],
        [
          'target_id' => '5',
          'target_revision_id' => '5',
        ],
        [
          'target_id' => '6',
          'target_revision_id' => '6',
        ],
        [
          'target_id' => '7',
          'target_revision_id' => '7',
        ],
      ],
    ];

    $this->assertEquals($expectedFormattedValues, $reflectionMethod->invoke($this->collectionFromNarrative));
  }

}
