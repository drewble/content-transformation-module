<?php

namespace Drupal\Tests\collection_from_entity\Unit;

/**
 * A mocked set of methods for entity operations.
 */
class EntityStub {
  /**
   * The expected introduction.
   *
   * @var string
   */
  private $introduction;

  /**
   * The expected id.
   *
   * @var string
   */
  private $id;

  /**
   * The expected bundle.
   *
   * @var string
   */
  private $bundle;

  /**
   * EntityStub constructor.
   *
   * @param string $title
   *   The provided title.
   * @param string $id
   *   The provided id.
   * @param string $bundle
   *   The provided bundle.
   */
  public function __construct($title, $id, $bundle) {
    $this->title = $title;
    $this->id = $id;
    $this->bundle = $bundle;
  }

  /**
   * Return the expected title.
   *
   * @return string
   *   The title.
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Return the expected bundle.
   *
   * @return string
   *   The bundle.
   */
  public function bundle() {
    return $this->bundle;
  }

  /**
   * Return the expected id.
   *
   * @return string
   *   The id.
   */
  public function id() {
    return $this->id;
  }

}
