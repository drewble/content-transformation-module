<?php

namespace Drupal\Tests\collection_from_entity\Unit;

use Drupal\collection_from_entity\Exception\PropertyNotFound;
use Drupal\locale\SourceString;
use Drupal\node\Entity\Node;

/**
 * Class NarrativeStub.
 *
 * @package Drupal\lonely_planet_collection\Tests\Stubs
 */
class NarrativeStub extends Node {

  /**
   * The expected title.
   *
   * @var string
   */
  private $title;

  // phpcs:disable
  /**
   * The expected content.
   *
   * @var string
   */
  private $field_nar_content;

  /**
   * The expected place.
   *
   * @var string
   */
  private $field_nar_place;
  // phpcs:enable

  /**
   * The expected owner.
   *
   * @var string
   */
  private $owner;

  /**
   * The expected introduction.
   *
   * @var string
   */
  private $introduction;

  /**
   * NarrativeStub constructor.
   *
   * @param string $title
   *   The provided title.
   * @param string $content
   *   The provided content.
   * @param string $introduction
   *   The provided intro.
   * @param string $owner
   *   The provided owner id.
   * @param string $place
   *   The provided place id.
   */
  public function __construct($title, $content, $introduction, $owner, $place) {
    $this->title = $title;
    $this->field_nar_content = (new SourceString())->setString($content);
    $this->owner = $owner;
    $this->introduction = $introduction;
    $this->field_nar_place = $place;
  }

  /**
   * Return the expected title.
   *
   * @return string
   *   The title.
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Return the requested property.
   *
   * @param string $propertyName
   *   The requested property name.
   *
   * @return |null
   *   The requested property.
   *
   * @throws \Drupal\collection_from_entity\Exception\PropertyNotFound
   */
  public function get($propertyName) {
    if ($this->$propertyName) {
      return $this->$propertyName;
    }
    else {
      throw new PropertyNotFound("Requested property ${propertyName} does not exist");
    }
  }

  /**
   * Return the expected owner ID.
   *
   * @return string
   *   The owner id.
   */
  public function getOwnerId() {
    return $this->owner;
  }

}
