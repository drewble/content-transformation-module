<?php

namespace Drupal\collection_from_entity\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\collection_from_entity\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Collection from entity settings form constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'lonely_planet_core.settings',
      'collection_from_entity.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'collection_from_entity_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('collection_from_entity.settings');
    $weights_range = range(-10, 10);
    $weights = array_combine($weights_range, $weights_range);

    $form['conversion_button_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text to use for the Conversion button'),
      '#description' => $this->t('This is the default text that will be used for the conversion button at the bottom of the node form.<br> Use a phrase that clearly indicates to the user that the content will be converted to a collection and the user will be redirected to the collection edit page.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('conversion_button_value'),
    ];
    $form['conversion_button_weight'] = [
      '#type' => 'select',
      '#title' => $this->t('Conversion Button Weight'),
      '#description' => $this->t('You can adjust the horizontal positioning in the button section.'),
      '#options' => $weights,
      '#default_value' => $config->get('conversion_button_weight'),
    ];

    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $keyed_node_types = [];
    foreach ($node_types as $content_type) {
      $keyed_node_types[$content_type->id()] = $content_type->label();
    }
    $form['node_types'] = [
      '#type' => 'checkboxes',
      '#options' => $keyed_node_types,
      '#title' => $this->t('Node types'),
      '#description' => $this->t('Set the node types you want to enable conversion for.'),
      '#default_value' => $config->get('node_types'),
      '#attributes' => ['disabled' => 'disabled'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('collection_from_entity.settings')
      ->set('conversion_button_value', $form_state->getValue('conversion_button_value'))
      ->set('conversion_button_weight', $form_state->getValue('conversion_button_weight'))
      ->save();
  }

}
