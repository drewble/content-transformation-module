<?php

namespace Drupal\collection_from_entity\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ClassResolver;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\collection_from_entity\CollectionFactory\CollectionFromNarrative;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Messenger\Messenger;

/**
 * Class CollectionFactoryController.
 */
class CollectionFactoryController extends ControllerBase {

  /**
   * The class_resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolver
   */
  private $classResolver;

  /**
   * The logger.factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManager $entityTypeManager, ClassResolver $classResolver, Messenger $messenger, AccountProxyInterface $currentUser, LoggerChannelFactoryInterface $logger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->classResolver = $classResolver;
    $this->messenger = $messenger;
    $this->currentUser = $currentUser;
    $this->logger = $logger->get('collection_from_entity');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('class_resolver'),
      $container->get('messenger'),
      $container->get('current_user'),
      $container->get('logger.factory')
    );
  }

  /**
   * Convert referring entity to Collection and route to edit page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request that hit the controller.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Where the client will go.
   */
  public function createCollectionFromReferringEntity(Request $request) : RedirectResponse {
    if ($this->currentUser && $this->currentUser->hasPermission('add collection entities') == FALSE) {
      $this->messenger->addError('Your account does not have permission to create collections.');
      return new RedirectResponse($request->get('referrer'));
    }

    if ($request->get('storageType') && $request->get('entityId')) {
      try {
        $collectionId = $this->createCollection($request->get('storageType'), $request->get('entityId'));
      }
      catch (InvalidPluginDefinitionException $e) {
        $message = "Could not create collection. \n ${e}";
        $this->messenger->addError($message);
        $this->logger->log('error', $message);
      }
      catch (PluginNotFoundException $e) {
        $message = "Could not create collection. \n ${e}";
        $this->messenger->addError($message);
        $this->logger->log('error', $message);
      }
    }

    return isset($collectionId) ? $this->redirectClientToEditForm($collectionId) : $this->redirectClientToReferrer($request);
  }

  /**
   * Convert entity to new Collection.
   *
   * @param string $storageType
   *   The storage type of the referring entity.
   * @param string $entityId
   *   The id of the referring entity.
   *
   * @return string
   *   The id of the new Collection entity.
   */
  protected function createCollection($storageType, $entityId) : string {
    try {
      $entity = $this->loadReferringEntity($storageType, $entityId);
    }
    catch (InvalidPluginDefinitionException $e) {
      throwException($e);
    }
    catch (PluginNotFoundException $e) {
      throwException($e);
    }
    $bundle = ucfirst($entity->bundle());
    $method = "createCollectionFrom${bundle}";
    return $this->$method($entity);
  }

  /**
   * Load the entity that began the process.
   *
   * @param string $storageType
   *   The storage type of the referring entity.
   * @param string $entityId
   *   The id of the referring entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The loaded entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadReferringEntity($storageType, $entityId) : EntityInterface {
    return $this->entityTypeManager->getStorage($storageType)->load($entityId);
  }

  /**
   * Create a new Collection from Narrative.
   *
   * @param \Drupal\Core\Entity\EntityInterface $narrative
   *   The narrative entity to transform.
   *
   * @return string
   *   The id of the newly created Collection.
   */
  protected function createCollectionFromNarrative(EntityInterface $narrative) : string {
    $collectionFromNarrative = $this->classResolver->getInstanceFromDefinition(CollectionFromNarrative::class);
    $collectionFromNarrative->load($narrative);
    return $collectionFromNarrative->createCollection();
  }

  /**
   * Redirect client to edit new collection.
   *
   * @param string $entity_id
   *   The id of the new Collection.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Where to send the client.
   */
  protected function redirectClientToEditForm($entity_id) : RedirectResponse {
    $message = 'Successfully created new Collection.';
    $this->messenger->addMessage($message);
    $this->logger->log('info', "${message} from entity id: ${entity_id}.");
    return $this->redirect("entity.collection.edit_form", ['collection' => $entity_id]);
  }

  /**
   * Return the client to previous address.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request that hit the controller.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Where to send the client.
   */
  protected function redirectClientToReferrer(Request $request) : RedirectResponse {
    $this->messenger->addError('Could not create Collection from entity.');
    return new RedirectResponse($request->get('referrer'));
  }

}
