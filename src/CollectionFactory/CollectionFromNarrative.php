<?php

namespace Drupal\collection_from_entity\CollectionFactory;

use Drupal\collection_from_entity\Exception\PropertyNotFound;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\lonely_planet_content\FormattedTextParser;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CollectionFromNarrative.
 *
 * @package Drupal\collection_from_entity\CollectionFactory
 */
class CollectionFromNarrative extends CollectionFromEntity {

  /**
   * Supported node types for Collection items.
   */
  const SUPPORTED_ITEM_TYPES = ['poi', 'place'];

  /**
   * Implement create method.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('lonely_planet_content.formatted_text_parser'),
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\lonely_planet_content\FormattedTextParser $formattedTextParser
   *   The formatted text parser service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory service.
   */
  public function __construct(FormattedTextParser $formattedTextParser, AccountProxyInterface $currentUser, Connection $database, EntityTypeManager $entityTypeManager, LoggerChannelFactoryInterface $logger) {
    $this->formattedTextParser = $formattedTextParser;
    $this->currentUser = $currentUser;
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger->get('collection_from_entity');
  }

  /**
   * Gets the Collection property.
   *
   * @inheritDoc
   */
  public function get($propertyName) {
    if ($this->$propertyName) {
      return $this->$propertyName;
    }
    else {
      throw new PropertyNotFound("Requested property ${propertyName} does not exist");
    }
  }

  /**
   * Creates a Collection.
   *
   * @inheritDoc
   */
  public function createCollection() {
    try {
      $newCollection = $this->entityTypeManager
        ->getStorage('collection')
        ->create($this->formatCollectionValues());
    }
    catch (InvalidPluginDefinitionException $e) {
      $this->logger->log('error', "Could not create collection. \n ${e}");
      echo $e;
    }
    catch (PluginNotFoundException $e) {
      $this->logger->log('error', "Could not create collection. \n ${e}");
      echo $e;
    }

    try {
      if ($newCollection->save()) {
        return $newCollection->id();
      }
    }
    catch (EntityStorageException $e) {
      $this->logger->log('error', "Could not create collection. \n ${e}");
      echo $e;
    }

    return FALSE;
  }

  /**
   * Takes an entity and breaks it into Collection properties.
   *
   * @inheritDoc
   */
  protected function explodeEntityIntoCollectionProperties($narrative) {
    $this->title = $this->getTitleFromEntity($narrative) ?? "New Collection " . date('m D, Y');
    $this->place = $this->getPlaceFromEntity($narrative) ?? "";
    $this->publisher = $this->currentUser->id() ?? $this->getPublisherFromEntity($narrative) ?? "";
    $this->introduction = $this->getIntroductionFromEntity($narrative) ?? "";
    $this->items = $this->getItemsFromEntityContent($narrative) ?? [];
  }

  /**
   * Gets the associated Place from an Entity.
   *
   * @inheritDoc
   */
  protected function getPlaceFromEntity($narrative) {
    return $narrative->get('field_nar_place');
  }

  /**
   * Gets the introduction from an Entity.
   *
   * @inheritDoc
   */
  protected function getIntroductionFromEntity($narrative) {
    $excerpt = $this->getExcerpt($narrative->get('field_nar_content')->getString());
    if ($this->hasAnchorChild($excerpt)) {
      return NULL;
    }

    return strip_tags($excerpt);

  }

  /**
   * Get everything to first closing tag.
   *
   * @param string $content
   *   The text blob to parse.
   *
   * @return false|string
   *   The first tag without a link in content.
   */
  protected function getExcerpt($content) {
    return $this->everythingToFirstClosingTag($content);
  }

  /**
   * Look for <a> tag in text.
   *
   * @param string $string
   *   The text blob to parse.
   *
   * @return false|int
   *   The position of the first <a>.
   */
  protected function hasAnchorChild($string) {
    return strpos($string, '<a');
  }

  /**
   * Return text up to the first closing tag.
   *
   * @param string $string
   *   The text blob to parse.
   *
   * @return false|string
   *   Everything to first closing tag.
   */
  protected function everythingToFirstClosingTag($string) {
    return substr($string, 0, $this->firstClosingTagPos($string));
  }

  /**
   * Find the first closing tag.
   *
   * @param string $string
   *   The text blob to parse.
   *
   * @return false|int
   *   The position of the first closing tag.
   */
  protected function firstClosingTagPos($string) {
    return strpos($string, '</');
  }

  /**
   * Gets the items from an Entity content field.
   *
   * @inheritDoc
   */
  protected function getItemsFromEntityContent($narrative) {
    $mentions = $this->getAllMentionsFromEntity($narrative->get('field_nar_content')->getString());
    foreach ($this->getEntitiesFromAllMentions($mentions) as $i => $entity) {
      yield new CollectionItem(
        $this->getItemDescriptionFromMention($mentions[$i]),
        $entity->id(),
        $entity->bundle(),
        $entity->getTitle()
      );
    }
  }

  /**
   * Gets all mentions from a text body.
   *
   * @inheritDoc
   */
  protected function getAllMentionsFromEntity($content) {
    return $this->formattedTextParser->getAllLinksWithContext($content);
  }

  /**
   * Gets Entities from mentions.
   *
   * @inheritDoc
   */
  protected function getEntitiesFromAllMentions($mentions) {
    foreach ($mentions as $mention) {
      yield $this->getEntity(
        $this->getEntityTypeFromMention($mention["path"]),
        $this->getExternalIdFromMention($mention["path"])
      );
    }
  }

  /**
   * Parse entity type from mention path.
   *
   * @param string $mention
   *   The mention to parse.
   *
   * @return string
   *   The type.
   */
  protected function getEntityTypeFromMention($mention) {
    return explode('/', ltrim($mention, '/'))[0];
  }

  /**
   * Parse external id from mention path.
   *
   * @param string $mention
   *   The mention to parse.
   *
   * @return string
   *   The external id.
   */
  protected function getExternalIdFromMention($mention) {
    return explode('/', ltrim($mention, '/'))[1];
  }

  /**
   * Gets an Entity by type and atlas id.
   *
   * @inheritDoc
   */
  protected function getEntity($entityType, $atlasId) {
    switch ($entityType) {
      case 'pois':
        return $this->fetchEntity('node__field_poi_external_id', 'field_poi_external_id_value', $atlasId);

      case 'places':
        return $this->fetchEntity('node__field_plc_external_id', 'field_plc_external_id_value', $atlasId);

      case 'narratives':
        return $this->fetchEntity('node__field_nar_guid', 'field_nar_guid_value', $atlasId);

      default:
        break;
    }

    return NULL;
  }

  /**
   * Retrieve entity from database.
   *
   * @param string $table
   *   The table where external id is stored.
   * @param string $column
   *   The column containing the external id value.
   * @param string $atlasId
   *   The external id to use in the where clause.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity retrieved from database.
   */
  protected function fetchEntity($table, $column, $atlasId) {
    $query = $this->database->select($table, 'base_table');
    $query->condition("base_table.${column}", $atlasId);
    $query->fields('base_table', ['entity_id']);
    $result = $query->execute();
    try {
      return $this->entityTypeManager->getStorage('node')->load($result->fetchField());
    }
    catch (InvalidPluginDefinitionException $e) {
      $this->logger->log('error', "Could not fetch entity from ${table}. \n Exception: ${e}");
      echo $e;
    }
    catch (PluginNotFoundException $e) {
      $this->logger->log('error', "Could not fetch entity from ${$table}. \n Exception: ${e}");
      echo $e;
    }

    return NULL;
  }

  /**
   * Get the item description from the mention.
   *
   * @inheritDoc
   */
  protected function getItemDescriptionFromMention($mention) {
    return $mention["context"];
  }

  /**
   * Creates a new Collection Item paragraph.
   *
   * @param CollectionItem $item
   *   The CollectionItem object to store in db.
   *
   * @return array|bool
   *   A target ID for new Collection Item paragraph.
   */
  protected function createItem(CollectionItem $item) {
    try {
      $paragraph = $this->entityTypeManager
        ->getStorage('paragraph')
        ->create([
          'type' => 'collection_item',
          'field_collection_content_ref' => $item->getEntityId(),
          'field_description' => $item->getDescription(),
        ]);
    }
    catch (InvalidPluginDefinitionException $e) {
      $this->logger->log('error', "Could not create Collection Item paragraph. \n Exception: ${e}");
      echo $e;
    }
    catch (PluginNotFoundException $e) {
      $this->logger->log('error', "Could not create Collection Item paragraph. \n Exception: ${e}");
      echo $e;
    }

    try {
      if ($paragraph->save()) {
        return [$paragraph->id(), $paragraph->getRevisionId()];
      }
    }
    catch (EntityStorageException $e) {
      $this->logger->log('error', "Could not save Collection Item paragraph. \n Exception: ${e}");
      echo $e;
    }

    return FALSE;
  }

  /**
   * Convert self properties to Collection properties.
   *
   * @return array
   *   Items formatted to match Collection properties.
   */
  protected function formatItemsForStorage() {
    return array_map(function ($item) {
      return array_combine(
        ['target_id', 'target_revision_id'],
        $this->createItem($item)
      );
    }, $this->getFilteredItems());
  }

  /**
   * Remove unsupported item types.
   *
   * @return array
   *   The array of supported items.
   */
  protected function getFilteredItems() {
    $supportedItems = [];
    foreach ($this->items as $item) {
      if ($this->isSupportedItemType($item)) {
        array_push($supportedItems, $item);
      }
    }

    return $supportedItems;
  }

  /**
   * Determine whether an item type is supported.
   *
   * @param CollectionItem $item
   *   The item to evaluate.
   *
   * @return bool
   *   Is the type supported.
   */
  protected function isSupportedItemType(CollectionItem $item) {
    if (in_array($item->getType(), self::SUPPORTED_ITEM_TYPES)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Prepare Collection storage values.
   *
   * @return array
   *   The collection values prepared for storage.
   */
  protected function formatCollectionValues() {
    return [
      'type' => 'collection',
      'name' => $this->title,
      'field_introduction' => $this->introduction,
      'field_collection_place' => $this->place,
      'field_collection_items' => $this->formatItemsForStorage(),
    ];
  }

}
