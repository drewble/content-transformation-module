<?php

namespace Drupal\collection_from_entity\CollectionFactory;

/**
 * Class CollectionItem.
 *
 * @package Drupal\collection_from_entity\CollectionFactory
 */
class CollectionItem {

  /**
   * The description.
   *
   * @var string
   */
  private $description;


  /**
   * The entity id.
   *
   * @var string
   */
  private $entityId;


  /**
   * The type.
   *
   * @var string
   */
  private $type;


  /**
   * The title.
   *
   * @var string
   */
  private $title;

  /**
   * CollectionItem constructor.
   *
   * @param string $description
   *   Reason for including item in collection.
   * @param string $entityId
   *   The primary key of the item.
   * @param string $type
   *   The type of item.
   * @param string $title
   *   The canonical title of the item.
   */
  public function __construct($description, $entityId, $type, $title) {
    $this->description = $description;
    $this->entityId = $entityId;
    $this->type = $type;
    $this->title = $title;
  }

  /**
   * Return description.
   *
   * @return string
   *   The description.
   */
  public function getDescription(): string {
    return $this->description;
  }

  /**
   * Return entity id.
   *
   * @return string
   *   The entity id.
   */
  public function getEntityId(): string {
    return $this->entityId;
  }

  /**
   * Return entity type.
   *
   * @return string
   *   The type.
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * Return title.
   *
   * @return string
   *   The title.
   */
  public function getTitle(): string {
    return $this->title;
  }

}
