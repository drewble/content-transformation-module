<?php

namespace Drupal\collection_from_entity\CollectionFactory;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides an interface for defining Collection entities.
 *
 * @property string $placeId
 * @property string $introduction
 * @property array $items
 * @property string $publisher
 */
interface CollectionFactoryInterface {

  /**
   * Gets the Collection property.
   *
   * @param string $propertyName
   *   The name of the property being accessed.
   *
   * @return string
   *   Returns specified property.
   *
   * @throws \Drupal\collection_from_entity\Exception\PropertyNotFound
   */
  public function get($propertyName);

  /**
   * Creates a Collection.
   *
   * @return string
   *   Returns the id of the Collection.
   */
  public function createCollection();

  /**
   * Extract values from arg entity into properties.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to transform into Collection.
   */
  public function load(EntityInterface $entity);

}
