<?php

namespace Drupal\collection_from_entity\CollectionFactory;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Class CollectionFromEntity.
 *
 * @package Drupal\collection_from_entity\CollectionFactory
 */
abstract class CollectionFromEntity implements CollectionFactoryInterface, ContainerInjectionInterface {

  /**
   * The narrative title.
   *
   * @var string
   */
  protected $title;

  /**
   * The narrative place value.
   *
   * @var string
   */
  protected $place;

  /**
   * The opening text of a narrative.
   *
   * @var string
   */
  protected $introduction;

  /**
   * The entities mentioned in the narrative.
   *
   * @var \Drupal\collection_from_entity\CollectionFactory\CollectionItem[]
   */
  protected $items;

  /**
   * Current user or creator of the narrative.
   *
   * @var string
   */
  protected $publisher;

  /**
   * The formatted_text_parser service.
   *
   * @var \Drupal\lonely_planet_content\FormattedTextParser
   */
  protected $formattedTextParser;

  /**
   * The current_user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The logger.factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Gets the Collection property.
   *
   * @inheritDoc
   */
  abstract public function get($propertyName);

  /**
   * Extract values from arg entity into properties.
   *
   * @inheritDoc
   */
  public function load($entity) {
    $this->explodeEntityIntoCollectionProperties($entity);
  }

  /**
   * Creates a Collection.
   *
   * @inheritDoc
   */
  abstract public function createCollection();

  /**
   * Gets the title from an Entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface $entity
   *  The entity to be parsed.
   *
   * @return string
   *   Returns the entity id string;
   */
  protected function getTitleFromEntity($entity) {
    return $entity->getTitle();
  }

  /**
   * Gets the associated Place from an Entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface $entity
   *  The entity to be parsed.
   *
   * @return string
   *   Returns the entity id string;
   */
  abstract protected function getPlaceFromEntity($entity);

  /**
   * Gets the publisher.
   *
   * @var \Drupal\Core\Entity\EntityInterface $entity
   *  The entity to be parsed.
   *
   * @return string
   *   Returns a publisher id.
   */
  protected function getPublisherFromEntity($entity) {
    return $entity->getOwnerId();
  }

  /**
   * Gets the introduction from an Entity.
   *
   * @var string $entity
   *  The entity to be parsed.
   *
   * @return string|null
   *   Returns introduction text.
   */
  abstract protected function getIntroductionFromEntity($entity);

  /**
   * Takes an entity and breaks it into Collection properties.
   *
   * @var \Drupal\Core\Entity\EntityInterface $entity
   *  The entity to be parsed.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   *  The entity to convert to collection.
   */
  abstract protected function explodeEntityIntoCollectionProperties($entity);

  /**
   * Gets the items from an Entity content field.
   *
   * @var \Drupal\Core\Entity\EntityInterface $entity
   *  The entity to be parsed.
   *
   * @return \Drupal\collection_from_entity\CollectionFactory\CollectionItem[]
   *   Returns array of items extracted from entity.
   */
  abstract protected function getItemsFromEntityContent($entity);

  /**
   * Gets all mentions from a text body.
   *
   * @var string $content
   *  Formatted text field content to be parsed.
   *
   * @return array
   *   Returns an array of mentions.
   */
  abstract protected function getAllMentionsFromEntity($content);

  /**
   * Gets Entities from mentions.
   *
   * @var array $mentions
   *  Every instance of a content reference.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Returns an array of Entities.
   */
  abstract protected function getEntitiesFromAllMentions($mentions);

  /**
   * Gets an Entity by type and atlas id.
   *
   * @var string $entityType
   *  The type of the entity.
   * @var string $externalId
   *  The primary key from external source.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Returns an Entity.
   */
  abstract protected function getEntity($entityType, $externalId);

  /**
   * Gets the item's description from the mention.
   *
   * @var array $mention
   *  The place in content where reference appears.
   *
   * @return string
   *   Returns a description string.
   */
  abstract protected function getItemDescriptionFromMention($mention);

}
