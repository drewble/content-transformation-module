<?php

namespace Drupal\collection_from_entity\Exception;

use Drupal\Driver\Exception\Exception;

/**
 * Define a custom exception class.
 */
class PropertyNotFound extends Exception {

}
