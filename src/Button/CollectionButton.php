<?php

namespace Drupal\collection_from_entity\Button;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class CollectionButton.
 *
 * @package Drupal\collection_from_entity\Button
 */
class CollectionButton {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  public $config;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  public $request;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * CollectionButton constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request stack service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user service.
   */
  public function __construct(ConfigFactoryInterface $config, RequestStack $request, AccountInterface $currentUser) {
    $this->config = $config;
    $this->request = $request;
    $this->currentUser = $currentUser;
  }

  /**
   * Return an instance to the service container.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   *
   * @return $this
   *   The object instance for the container.
   */
  public function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('request_stack'),
      $container->get('current_user')
    );
  }

  /**
   * Determines whether conversion button can be inserted.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form array.
   */
  public function maybeInsertCollectionButton(array &$form, FormStateInterface $form_state) : array {
    if ($this->currentUser && $this->currentUser->hasPermission('add collection entities') == FALSE) {
      return $form;
    }

    $moduleConfig = $this->getConfig();
    if ($this->isNotNewEntity($form_state) && $this->typeIsConfiguredForCollectionFromEntity($form_state, $moduleConfig)) {
      $this->insertButton($form, $form_state, $moduleConfig);
    }

    return $form;
  }

  /**
   * Read the module config settings.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The config object.
   */
  protected function getConfig() : ImmutableConfig {
    return $this->config->get('collection_from_entity.settings');
  }

  /**
   * Checks if entity already exists.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return bool
   *   Determines whether entity is new or not.
   */
  protected function isNotNewEntity(FormStateInterface $form_state) : bool {
    return $this->getEntity($form_state)->isNew() ? FALSE : TRUE;
  }

  /**
   * Checks if current entity has conversion support.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param \Drupal\Core\Config\ImmutableConfig $moduleConfig
   *   The config object for this module.
   *
   * @return bool
   *   Determines whether entity type is supported.
   */
  protected function typeIsConfiguredForCollectionFromEntity(FormStateInterface $form_state, ImmutableConfig $moduleConfig) : bool {
    return in_array($this->getEntityType($form_state), $this->getSupportedNodeTypes($moduleConfig), TRUE);
  }

  /**
   * Retrieve the entity type from the entity.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return string
   *   The type of the entity.
   */
  protected function getEntityType(FormStateInterface $form_state) : string {
    return $this->getEntity($form_state)->getType();
  }

  /**
   * Retrieve the entity from the form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity represented by the form object.
   */
  protected function getEntity(FormStateInterface $form_state) : EntityInterface {
    return $form_state->getFormObject()->getEntity();
  }

  /**
   * Check config for supported Node types.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $moduleConfig
   *   The config object for this module.
   *
   * @return array
   *   The supported Node types.
   */
  protected function getSupportedNodeTypes(ImmutableConfig $moduleConfig) : array {
    return array_values($moduleConfig->get('node_types'));
  }

  /**
   * Inserts a button into the form object.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param \Drupal\Core\Config\ImmutableConfig $moduleConfig
   *   The config object for this module.
   */
  protected function insertButton(array &$form, FormStateInterface $form_state, ImmutableConfig $moduleConfig) : void {
    $form['actions'] = array_merge(['collection' => $this->createButton($form_state, $moduleConfig)], $form['actions']);
  }

  /**
   * Assembled the button to insert into the form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param \Drupal\Core\Config\ImmutableConfig $moduleConfig
   *   The config object for this module.
   *
   * @return array
   *   The formatted button to add to form.
   */
  protected function createButton(FormStateInterface $form_state, ImmutableConfig $moduleConfig) : array {
    return [
      '#type' => "link",
      '#title' => $moduleConfig->get('conversion_button_value'),
      '#access' => TRUE,
      '#attributes' => [
        "class" => [
          "button",
        ],
      ],
      '#url' => new Url('collection_from_entity.collection_factory',
        [
          'storageType' => $this->getEntity($form_state)->getEntityTypeId(),
          'entityId' => $this->getEntity($form_state)->id(),
          'referrer' => $this->request->getCurrentRequest()->getRequestUri(),
        ]
      ),
      '#weight' => intval($moduleConfig->get('conversion_button_weight')),
      '#button_type' => "primary",
    ];
  }

}
